#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;



int miseCorrecte (int miseJoueur,int nbJetons) //fonction pour savoir si la mise est correcte
{
    //printf("Combien voulez vous misez, entre 1 et 25 ?\n");
    cout << "Combien voulez vous misez, entre 1 et 25 ?" << endl;
    cin >> miseJoueur;

    while (miseJoueur > 25 || miseJoueur< 1 || miseJoueur>nbJetons)
    {
        if (miseJoueur>nbJetons)
        {
            cout << "Vous n'avez pas assez de jetons" << endl;
            //printf("Vous n'avez pas assez de jetons\n\n");
        }
        cout << "Il faut miser entre 1 et 25. Combien voulez vous misez ?" << endl;
        //printf("Il faut miser entre 1 et 25\nCombien voulez vous misez ?\n");
        cin >> miseJoueur;
    }
    return miseJoueur;

}




int choixCorrect(int choixJoueur) // fonction pour savoir si le choix du joueur est correct
{
    cout << "Selectionnez [0] pour impair, [1] pour pair" << endl;
    //printf("\nSelectionnez [0] pour impair, [1] pour pair\n");
    cin >> choixJoueur;

    while (choixJoueur > 1 || choixJoueur <0)
    {
        cout << "Il faut choisir [0] pour impair, [1] pouur pair ?" << endl;
        //printf("Il faut choisir [0] pour impair, [1] pouur pair ?\n");
        cin >> choixJoueur;
    }
    return choixJoueur;
}





int main()
{
    srand(time(NULL));

    int nbJetons = 10;
    int choixJoueur = 0;
    int miseJoueur = 0;
    int pressDetente = 1;
    int slotBarrilet = 5; //indique combien de slot il y a dans la barrilet
    int balle = rand()%5;
    bool continuRoull = true; // continuer ou non la roulette russe
    bool vieJoueur = true;
    bool continuer = true;// sert a sortir de ma boucle  while avec un bool



    while (vieJoueur) //tant que le joueur est en vie, on joue
    {

        while(continuer == true)
        {
            continuRoull= true; //sert plus tard, si le joueur dois passer de la roulette a la roulette russe plusieurs fois

            if (nbJetons >= 100) //si le nombre de jetons est superieur a 100, on sort de la boucle
            {
                continuer = false;
                vieJoueur = false;
                cout <<"Vous avez les 100 jetons, vous pouvez partir !"<<endl;
            }


            else if (nbJetons <= 0) //si le nombre de jetons est inferieur ou egal a 0, on sort de la boucle
            {
                continuer = false;
            }


            else if (nbJetons < 100) //avec les deux conditions plus haut, celle ci s'applique quand le nombre de jetons est entre 1 et 100
            {
                int nbRoue = rand()%2;


                miseJoueur=miseCorrecte(miseJoueur,nbJetons); //vérification si la mise du joueur est entre 1 et 25
                choixJoueur=choixCorrect(choixJoueur); //Vérif si le choix du joueur entre pair et impair est correct



                if (choixJoueur%2==nbRoue)
                {
                    cout<< "La boule tombe sur, " <<nbRoue<< " vous gagnez !"<<endl;
                    nbJetons = nbJetons + miseJoueur * 2;
                }

                else
                {
                    cout<<"La roue tombe sur le "<<nbRoue<<" vous perdez votre mise"<< endl;
                    nbJetons = nbJetons - miseJoueur;
                }
                cout<<"Vous avez maintenant "<< nbJetons<<" jetons"<<endl;

            }

            else // pour sortir de la boucle si on tombe sur un cas pas prevu et qui rentre pas dans les conditions du dessus.
            {
                continuer =false;
            }





            //Si le joueur n'as plus de jetons, on passe a la roulettre russe
            if (nbJetons<=0)
            {
                cout << "Comme n'avez plus de jetons, il faut jouer a la roulette Russe pour en gagner"<< endl;

                while (continuRoull)
                {

                    cout<< "Appuyer sur [0] pour presser la detente." << endl;
                    cin >> pressDetente;


                    while (pressDetente !=0) //Pour avoir une action entre le joueur et l'ordi lorsqu'on doit tirer
                    {
                        cout<< "Appuyer sur [0] pour presser la detente."<< endl;
                        cin >> pressDetente;
                    }


                    if ( slotBarrilet == balle ) //le cas ou le joueur meurt
                    {

                        cout << "La balle etait dans la chambre numero "<< slotBarrilet<<" et vous etes tombe sur la chambre... "<<balle<<endl;
                        continuRoull = false;
                        vieJoueur = false;
                        continuer = false;
                        //permet de sortir des boucles car joueur mort, donc fin de programme
                    }

                    else
                    {
                        cout<<"Par chance, la balle n'etait pas dans la chambre"<<endl;
                        cout<<"(La balle etait dans la chambre numero " << slotBarrilet<< " et vous etes tombe sur la chambre... "<<balle<<endl;
                        nbJetons = nbJetons + 20;  // si le joueur ne meurt pas, il gagne 02 jetons
                        slotBarrilet = slotBarrilet -1;
                        cout <<"Vous gagnez 20 Jetons\nVous en avez maintenant "<< nbJetons << endl;
                        if(nbJetons >= 100)
                        {
                            cout<<"Grace a votre prise de risque, vous avez vos 100 jetons."<<endl;
                            cout<<"Selectionner [0] pour continuer a risquer votre vie a la roulette russe."<<endl;
                            cout<<"Ou [1] pour echanger vos jetons contre votre liberte"<<endl;
                        }

                        else
                        {
                            cout<<"Voulez vous continuer la roullette Russe, ou retourner miser vos jetons sur la roulette ?"<<endl;
                            cout<<"[0] pour continuer. [1] pour se diriger vers la roulette"<<endl;
                        }

                        cin >> continuRoull;



                        switch (continuRoull)
                        {
                        case 0:
                            continuRoull = true;
                            break;

                        case 1:
                            continuRoull = false;
                            break;
                        }

                    }

                }



            }

        }
    }


    if (nbJetons >= 100)
    {
        cout<<"Felicitation Monsieur Bond, vous vous en etes sortis face aux Communistes"<<endl;
    }

    else if (vieJoueur == false)
    {
        cout << "\nVous mourrez" << endl;
    }


    return 0;
}
